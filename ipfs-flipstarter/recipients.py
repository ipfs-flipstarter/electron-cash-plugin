#!/usr/bin/env python3
#
# Electrum - lightweight Bitcoin client
# Copyright (C) 2012 thomasv@gitorious
#
# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation files
# (the "Software"), to deal in the Software without restriction,
# including without limitation the rights to use, copy, modify, merge,
# publish, distribute, sublicense, and/or sell copies of the Software,
# and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
# BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
# ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import QCompleter, QPlainTextEdit

import re
import sys
from decimal import Decimal as PyDecimal  # Qt 5.12 also exports Decimal
from electroncash_gui.qt.qrtextedit import ScanQRTextEdit
from electroncash import bitcoin
from electroncash.address import Address, ScriptOutput
from electroncash import networks
from electroncash.util import PrintError
from electroncash.contacts import Contact
from electroncash import web

from electroncash_gui.qt import util
from electroncash_gui.qt.main_window import ElectrumWindow
from electroncash_gui.qt.util import ButtonsTextEdit, MessageBoxMixin, ColorScheme

RE_ALIAS = r'^(.*?)\s*<\s*([0-9A-Za-z:]{26,})\s*>$'
RE_AMT = r'^.*\s*,\s*([0-9,.]*)\s*$'

RX_ALIAS = re.compile(RE_ALIAS)
RX_AMT = re.compile(RE_AMT)

frozen_style = "PayToEdit { border:none;}"
normal_style = "PayToEdit { }"

class Recipients(PrintError, ButtonsTextEdit, MessageBoxMixin):

    def __init__(self, win):
        assert isinstance(win, ElectrumWindow) and win.amount_e and win.wallet
        ButtonsTextEdit.__init__(self)
        MessageBoxMixin.__init__(self)
        self.win = win
        self.amount_edit = win.amount_e
        document = self.document()
        document.contentsChanged.connect(self.update_size)

        fontMetrics = QFontMetrics(document.defaultFont())
        self.fontSpacing = fontMetrics.lineSpacing()

        margins = self.contentsMargins()
        documentMargin = document.documentMargin()
        self.verticalMargins = margins.top() + margins.bottom()
        self.verticalMargins += self.frameWidth() * 2
        self.verticalMargins += int(documentMargin * 2)

        self.heightMin = self.fontSpacing + self.verticalMargins
        self.heightMax = (self.fontSpacing * 10) + self.verticalMargins

        self.c = None
        self.setReadOnly(True)
        self.textChanged.connect(self.check_text)
        self.outputs = []
        self.errors = []
        self.scan_f = win.pay_to_URI
        self.update_size()
        self.payto_address = None
        self._original_style_sheet = self.styleSheet() or ''

        self.previous_payto = ''

        if sys.platform in ('darwin',):
            # See issue #1411 -- on *some* macOS systems, clearing the
            # payto field with setText('') ends up leaving "ghost" pixels
            # in the field, which look like the text that was just there.
            # This situation corrects itself eventually if another repaint
            # is issued to the widget. I couldn't figure out why it is happening
            # and the workaround is simply to force a repaint using this trick
            # for all textChanged events. -Calin
            self.textChanged.connect(self.repaint)

        self.verticalScrollBar().valueChanged.connect(self._vertical_scroll_bar_changed)

    def setFrozen(self, b):
        self.setReadOnly(b)
        self.setStyleSheet(self._original_style_sheet + (frozen_style if b else normal_style))
        self.overlay_widget.setHidden(b)

    def setGreen(self):
        if sys.platform in ('darwin',) and util.ColorScheme.dark_scheme:
            # MacOS dark mode requires special treatment here
            self.setStyleSheet(self._original_style_sheet + util.ColorScheme.DEEPGREEN.as_stylesheet(True))
        else:
            self.setStyleSheet(self._original_style_sheet + util.ColorScheme.GREEN.as_stylesheet(True))

    def setExpired(self):
        self.setStyleSheet(self._original_style_sheet + util.ColorScheme.RED.as_stylesheet(True))

    def parse_address_and_amount(self, line):
        x, y = line.split(',')
        out_type, out = self.parse_output(x)
        amount = self.parse_amount(y)
        return out_type, out, amount

    @classmethod
    def parse_output(cls, x):
        web.parse_URI(x, strict=True)
        address = cls.parse_address(x)
        return bitcoin.TYPE_ADDRESS, address

    @staticmethod
    def parse_address(line):
        r = line.strip()
        m = RX_ALIAS.match(r)
        address = m.group(2) if m else r
        return Address.from_string(address)

    def parse_amount(self, x):
        if x.strip() == '!':
            return '!'
        satoshis = PyDecimal(x.strip())
        bitcoin = satoshis / pow(10, 8)
        p = pow(10, self.amount_edit.decimal_point())
        return int(p * bitcoin)
        
    def calculate_total_fee(self, recipient_count):
        average_byte_per_recipient = 69
        transaction_metadata_bytes = 10
        return (
            (average_byte_per_recipient * recipient_count) +
            transaction_metadata_bytes
        )

    def check_text(self):
        self.errors = []
        # filter out empty lines
        lines = [i for i in self.lines() if i]
        outputs = []
        total = 0
        self.payto_address = None

        is_max = False
        for i, line in enumerate(lines):
            try:
                _type, to_address, amount = self.parse_address_and_amount(line)
            except:
                self.errors.append((i, line.strip()))
                continue

            outputs.append((_type, to_address, amount))
            if amount == '!':
                is_max = True
            else:
                total += amount

        self.win.max_button.setChecked(is_max)

        self.outputs = outputs
        self.total_amount = total + self.calculate_total_fee(len(outputs)) if outputs else None
        self.payto_address = outputs[0] if len(outputs) > 0 else None

        if self.win.max_button.isChecked():
            self.win.do_update_fee()
        else:
            self.amount_edit.setAmount(self.total_amount)
            self.win.lock_amount(total or len(lines)>1)

    def get_errors(self):
        return self.errors

    def get_recipient(self):
        return self.payto_address

    def get_recipient_address(self):
        return self.payto_address[1] if self.payto_address else None

    def get_recipient_addresses(self):
        return list(map(lambda x:str(x[1]), self.outputs))

    def get_outputs(self):
        return list(self.outputs)
        
    def lines(self):
        return self.toPlainText().split('\n')

    def is_multiline(self):
        return len(self.lines()) > 1

    def paytomany(self):
        self.setText("\n\n\n")
        self.update_size()

    def update_size(self):
        docLineCount = self.document().lineCount()
        if self.cursorRect().right() + 1 >= self.overlay_widget.pos().x():
            # Add a line if we are under the overlay widget
            docLineCount += 1
        docHeight = docLineCount * self.fontSpacing

        h = docHeight + self.verticalMargins
        h = min(max(h, self.heightMin), self.heightMax)

        self.setMinimumHeight(h)
        self.setMaximumHeight(h)

        self.verticalScrollBar().setHidden(docHeight + self.verticalMargins < self.heightMax)

        # The scrollbar visibility can have changed so we update the overlay position here
        self._updateOverlayPos()

    def _vertical_scroll_bar_changed(self, value):
        ''' Fix for bug #1521 -- Contents of payto edit can disappear
        unexpectedly when selecting with mouse on a single-liner. '''
        vb = self.verticalScrollBar()
        docLineCount = self.document().lineCount()
        if docLineCount == 1 and vb.maximum()-vb.minimum() == 1 and value != vb.minimum():
            self.print_error(f"Workaround #1521: forcing scrollbar value back to {vb.minimum()} for single line payto_e.")
            vb.setValue(vb.minimum())

    def setCompleter(self, completer):
        self.c = completer
        self.c.setWidget(self)
        self.c.setCompletionMode(QCompleter.PopupCompletion)
        self.c.activated.connect(self.insertCompletion)


    def insertCompletion(self, completion):
        if self.c.widget() != self:
            return
        tc = self.textCursor()
        # new! because of the way Cash Accounts works we must delete the whole
        # line under cursor and insert the full completion. This ends up
        # working reasonably well.
        tc.select(QTextCursor.LineUnderCursor)
        tc.removeSelectedText()
        tc.insertText(completion + " ")
        self.setTextCursor(tc)


    def textUnderCursor(self):
        tc = self.textCursor()
        tc.select(QTextCursor.LineUnderCursor)
        return tc.selectedText()

    def keyPressEvent(self, e):
        if self.isReadOnly() or not self.hasFocus():
            e.ignore()
            return

        if self.c.popup().isVisible():
            if e.key() in [Qt.Key_Enter, Qt.Key_Return]:
                e.ignore()
                return

        if e.key() in [Qt.Key_Tab, Qt.Key_Backtab]:
            e.ignore()
            return

        if e.key() in [Qt.Key_Down, Qt.Key_Up] and not self.is_multiline():
            e.ignore()
            return

        super().keyPressEvent(e)

        ctrlOrShift = e.modifiers() and (Qt.ControlModifier or Qt.ShiftModifier)
        if self.c is None or (ctrlOrShift and not e.text()):
            return

        hasModifier = (e.modifiers() != Qt.NoModifier) and not ctrlOrShift
        completionPrefix = self.textUnderCursor()

        if hasModifier or not e.text() or len(completionPrefix) < 1:
            self.c.popup().hide()
            return

        if completionPrefix != self.c.completionPrefix():
            self.c.setCompletionPrefix(completionPrefix)
            self.c.popup().setCurrentIndex(self.c.completionModel().index(0, 0))

        cr = self.cursorRect()
        cr.setWidth(self.c.popup().sizeHintForColumn(0) + self.c.popup().verticalScrollBar().sizeHint().width())
        self.c.complete(cr)

    def qr_input(self):
        def _on_qr_success(result):
            if result and result.startswith(networks.net.CASHADDR_PREFIX + ":"):
                self.scan_f(result)
                # TODO: update fee
        super(PayToEdit,self).qr_input(_on_qr_success)
