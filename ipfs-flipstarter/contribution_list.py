#!/usr/bin/env python3
#
# Electrum - lightweight Bitcoin client
# Copyright (C) 2015 Thomas Voegtlin
#
# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation files
# (the "Software"), to deal in the Software without restriction,
# including without limitation the rights to use, copy, modify, merge,
# publish, distribute, sublicense, and/or sell copies of the Software,
# and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
# BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
# ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from electroncash.address import Address
from electroncash.i18n import _
from electroncash.util import format_time, age
from electroncash.plugins import run_hook
from electroncash.paymentrequest import pr_tooltips, PR_UNKNOWN
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import QTreeWidgetItem, QMenu, QAbstractItemView
from electroncash_gui.qt.util import MyTreeWidget, pr_icons


class ContributionList(MyTreeWidget):
    filter_columns = [0, 1, 2, 3, 4]  # Date, Account, Address, Description, Amount


    def __init__(self, parent):
        MyTreeWidget.__init__(self, parent, self.create_menu, [_('Height'), _('Alias'), _('Comment'), _('Amount'), _('Tx Hash'), _('Data Hash'), _('Status')], 4, deferred_updates=False)
        self.setSelectionMode(QAbstractItemView.MultiSelection)
        self.setSortingEnabled(True)
        self.setColumnWidth(0, 180)
        self.wallet = parent.wallet

    def select_item_by_address(self, address):
        pass

    def on_edited(self, item, column, prior):
        '''Called only when the text in the memo field actually changes.
        Updates the UI and re-saves the request. '''
        print("on_edited")

    def on_update(self):
        print("on_update")

    def create_menu(self, position):
        item = self.itemAt(position)
        if not item:
            return
        self.setCurrentItem(item)  # sometimes it's not the current item.
        addr = item.data(0, Qt.UserRole)
        column = self.currentColumn()
        column_title = self.headerItem().text(column)
        column_data = item.text(column)
        menu = QMenu(self)
        # TODO God willing: Copy/open CID, save as JSON File
        menu.addAction(_("Copy {}").format(column_title), lambda: self.parent.app.clipboard().setText(column_data.strip()))
        menu.addAction(_("Copy CID URL"), lambda: None)
        menu.addAction(_("Save as JSON"), lambda: None)
        menu.exec_(self.viewport().mapToGlobal(position))
